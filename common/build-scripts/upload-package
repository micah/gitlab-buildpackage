#!/bin/bash -x

# exit on errors
set -e

# shellcheck disable=SC1091
. /usr/local/sbin/build-lib

# shellcheck disable=SC1091
. /usr/local/sbin/build-env

elog "Uploading to repository ..."
FILES=$(find . -type f -maxdepth 1)
UKHF=$(mktemp -d /tmp/ukh.XXX)/ukhfile

# Configure ssh private from SSH_PRIVATE_KEY
# environment variable if it is set
# DPrevent output of SSH_PRIVATE_KEY to gitlab Ci console
# by disabling xtrace
[[ "$-" =~ 'x' ]] && xtrace=true || xtrace=false
set +x
if [ -n "$SSH_PRIVATE_KEY" ]
then
  /bin/echo "$SSH_PRIVATE_KEY" > /etc/dockerbuild.id_rsa
  chmod 0600 /etc/dockerbuild.id_rsa
fi
[[ $xtrace == true ]] && set -x

REPOREMOTE=${REPOREMOTE:=dockerbuild@repo.example.org}
SSH_OPTS=(-o "UserKnownHostsFile=$UKHF")
REPOREMOTE_OPTS_DEFAULTS=(-o 'StrictHostKeyChecking=no' -i '/etc/dockerbuild.id_rsa')
if [ -z "$REPOREMOTE_OPTS" ]
then
  REPOREMOTE_OPTS=( ${REPOREMOTE_OPTS_DEFAULTS[@]} )
else
  REPOREMOTE_OPTS=( ${REPOREMOTE_OPTS[@]} )
fi

# Replace -p with -P for scp
REPOREMOTE_SCP_OPTS=( ${REPOREMOTE_OPTS[@]/-p/-P} )

echo "REPOREMOTE_OPTS:"
echo "${REPOREMOTE_OPTS[@]}"
echo "REPOREMOTE_SCP_OPTS:"
echo "${REPOREMOTE_SCP_OPTS[@]}"

on_repo_target() {
  # Ignore warning that this expands on client side
  # shellcheck disable=SC2029
  ssh "${REPOREMOTE_OPTS[@]}" "${SSH_OPTS[@]}" "$REPOREMOTE" "$@"
}
MYTMP=$(on_repo_target mktemp -d /tmp/incomingXXXXXX)
for filename in $FILES results/target-distribution; do
  scp "${REPOREMOTE_SCP_OPTS[@]}" "${SSH_OPTS[@]}" \
    "$filename" \
    "${REPOREMOTE}:${MYTMP}/"
done

# Include uploaded .debs into one or more repositories
for repo in $(echo "$REPONAMES" | tr ',' ' ')
do
  on_repo_target /bin/bash -x "/usr/local/bin/incoming-dockerbuild ${MYTMP} ${REPOBASEDIR}/${repo} ${DIST}"
done

# Move back (most) files from source/ so users can use them as artificats.
mv source/* source/.git* .
